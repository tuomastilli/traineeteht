# traineetehtava

This app allows the user to look up the current temperature of a desired city. It was built using Java, Maven and Spring-boot framework. The app's internal structure attempts to follow the model-view-controller architecture.

## How to use:

Clone the repository

Run the program (easiest way: navigate to project folder in terminal and use .mvnw spring-boot:run)

Open a browser and visit http://localhost:8080/

Type your desired city into the search form and click submit!

## Tests:

There are also a few basic tests available. ApplicationTest checks that the context loads properly and a Controller instance can be found. JsonTests class checks that JSON serialization/deserialization works. ServerTest tests that the main page works.