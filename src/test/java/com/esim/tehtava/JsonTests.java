package com.esim.tehtava;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.boot.test.json.*;
import org.springframework.test.context.junit4.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
public class JsonTests {

    private JacksonTester<WeatherObject> json;
    
    @Before
    public void setup() {
        json.initFields(this, new ObjectMapper());
    }
    
    @Test
    public void testSerialize() throws Exception {
        TempObject t = new TempObject();
        t.setTemp((Double)(10.50));
        WeatherObject details = new WeatherObject(65, "kaupunki", t);
        assertThat(this.json.write(details)).extractingJsonPathNumberValue("@.id")
        		.isEqualTo(65);
        assertThat(this.json.write(details)).extractingJsonPathStringValue("@.name")
                .isEqualTo("kaupunki");
        assertThat(this.json.write(details)).extractingJsonPathNumberValue("@.main.temp")
                .isEqualTo(10.50);
    }

    @Test
    public void testDeserialize() throws Exception {
        String content = "{\"main\":{\"temp\":15.00,\"temp_min\":5.20,\"temp_max\":14.70},\"id\":18,\"name\":\"city\"}";
        
        assertThat(this.json.parseObject(content).getId()).isEqualTo(18);
        assertThat(this.json.parseObject(content).getTempObject().getTemp()).isEqualTo(15);
    }

}