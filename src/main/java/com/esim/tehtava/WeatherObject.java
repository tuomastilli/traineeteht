package com.esim.tehtava;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherObject {
    private int id;
    private String name;

    @JsonProperty("main")
    private TempObject tempObject;

    public WeatherObject(int id, String name, TempObject tempObject) {
        this.id = id;
        this.name = name;
        this.tempObject = tempObject;
    }

    public WeatherObject() {
    }

    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public TempObject getTempObject() {
        return tempObject;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setTempObject(TempObject tempObject) {
        this.tempObject = tempObject;
    }
    @Override
    public String toString() {
        return "WeatherObject{" +
                "id='" + id + '\'' +
                ", name=" + name +
                ", tempObject=" + tempObject +
                '}';
    }
}