package com.esim.tehtava;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

@Controller
public class SearchController {
    @GetMapping("/search")
    public String search(@RequestParam(name="city") String city, Model model) {
        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=5ae0c10c641ee68b087dee6ad0ddf940";

        try {
            ResponseEntity<WeatherObject> response = restTemplate.getForEntity(resourceUrl, WeatherObject.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                WeatherObject weatherObject = response.getBody();
                double temperature = weatherObject.getTempObject().getTemp();
                String cityName = weatherObject.getName();
                model.addAttribute("temp", temperature);
                model.addAttribute("cityname", cityName);
            }
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            model.addAttribute("temp", "-");
            model.addAttribute("cityname", "Not found");
        }
        return "search";
    }

}