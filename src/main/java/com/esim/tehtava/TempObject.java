package com.esim.tehtava;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TempObject {
    private double temp;

    public TempObject(double temp) {
        this.temp = temp;
    }
    public TempObject() {
    }

    public Double getTemp(){
        return temp;
    }
    public void setTemp(double temp){
        this.temp = temp;
    }

    @Override
    public String toString() {
        return "TempObject{" +
                "temperature=" + temp + '\'' +
                '}';
    }
}